HISTFILE="$HOME/.zshistory"
SAVEHIST=100000000

HISTSIZE=10000

TIMEFMT="%J: user %U sys %S cpu %P mem-max %M in %I out %O recv %r sent %s ela %*E"

setopt INC_APPEND_HISTORY
#setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_VERIFY

setopt MARK_DIRS
setopt PRINT_EXIT_VALUE
setopt AUTO_PUSHD
setopt EXTENDED_HISTORY

if [ -z "$SSH_CLIENT" ]; then
	PS1="$(print '%{\e[1;32m%}%n@%M %{\e[1;39m%}[%~]%{\e[1;32m%}#%{\e[0m%}') "
else
	PS1="$(print '%{\e[1;33m%}%n@%M %{\e[1;39m%}[%~]%{\e[1;32m%}#%{\e[0m%}') "
fi

RPS1=$(print "%{\e[1;30m%}%D{%H:%M:%S}%{\e[0m%}")

setopt No_Beep

autoload -U compinit
compinit -C

# formatting and messages
zstyle ':completion:*' verbose no
zstyle ':completion:*:descriptions' format '%B%d%b'
#zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

zstyle ':completion:*:default' list-colors 'no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;31:'

source ~/.zsh/env

if [ -f ~/.zsh/user/"$USER" ]; then
	source ~/.zsh/user/"$USER"
fi

platform=$(uname | tr '[:upper:]' '[:lower:]')
if [ -f ~/.zsh/platform/"$platform" ]; then
	source ~/.zsh/platform/"$platform"
fi

hostname=$(hostname)
if [ -f ~/.zsh/host/"$hostname" ]; then
	source ~/.zsh/host/"$hostname"
fi
